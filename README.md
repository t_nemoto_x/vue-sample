# vuex-test

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve server-side mock by json-server
npm run mock

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test by karma with mocha
npm test
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
