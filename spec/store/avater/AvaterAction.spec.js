import axios from '@lib/axios';
import sinon from '@lib/sinon';
import assert from '@lib/power-assert';
import {AvaterActions} from '@/store/modules/avater/AvaterActions';
import MockAdapter from '@lib/axios-mock-adapter';

// describeはテストをいずれかの単位で括るだけ
describe('AvaterActions', () => {

  let mock = new MockAdapter(axios);

  // describeはこんな感じでネストできるのでわかりやすい階層構造にするとよい
  describe('selectUser', () => {

    describe('正常系', () => {
      // spyのオブジェクト生成
      let spySelectUser = sinon.spy(AvaterActions, 'selectUser');

      // 戻り値の期待値
      const expectedPayload = {
        userinfo: {
          username: 'testName',
          avaterUrl: 'testUrl'
        }
      };

      // すべてのテストの前に1回だけ呼び出される前処理
      before(() => {
        mock.onGet('http://localhost:3000/userinfo').reply(200, {
          data: [
            expectedPayload
          ]
        });
      });

      // 各テストの後に毎回呼び出される後処理
      afterEach(() => {
        // spyの初期化
        spySelectUser.restore();
      });


      it('Mutationに渡す引数がuserinfo以下であることを確認する', (done) => {
        // commitに渡した値を確認するためにcommitにassertを忍ばせたオブジェクトを生成
        const context = {
          commit: (type, payload) => {
            assert.equal(type, 'updateAvater');
            assert.deepEqual(payload, expectedPayload);
          }
        };

        // 上記のオブジェクトを詰めてテスト対象関数を呼び出し
        AvaterActions.selectUser(context, 'test');

        // 特に不要だけどサンプルとして。
        // オブジェクトの関数をスパイして、引数とか戻り値とかを確認できる
        assert.equal(spySelectUser.getCall(0).args[1], 'test');
        done();
      });

    });
  });
});
