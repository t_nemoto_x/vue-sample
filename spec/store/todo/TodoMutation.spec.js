import {TodoMutations} from '@/store/modules/todo/TodoMutations';
import assert from '@lib/power-assert';

// describeはテストをいずれかの単位で括るだけ
describe('TodoMutations', () => {

  // 初期状態
  let DEFAULT_STATE = {
    items: [
      {title: 'Task1', is_do: false},
      {title: 'Task2', is_do: true},
      {title: 'Task3', is_do: false},
    ]
  };

  // describeはこんな感じでネストできるのでわかりやすい階層構造にするとよい
  describe('addTask', () => {
    it('新規タスクが追加されることを確認する', () => {
      let newTask = {title: 'newTask', is_do: false};
      let state = Object.assign({}, DEFAULT_STATE);
      TodoMutations.addTask(state, { data: newTask });
      assert.equal(state.items.length, 4);
      assert.deepEqual(state.items[3], newTask);
    });
  });

  // ここでは関数ごとにわけてネストしている
  describe('doneTask', () => {
    it('未完了のタスクが完了することを確認する', () => {
      let state = Object.assign({}, DEFAULT_STATE);
      TodoMutations.doneTask(state, { data: DEFAULT_STATE.items[0]});
      assert.equal(state.items[0].is_do, true);
    });

    it('完了のタスクが未完了になることを確認する', () => {
      let state = Object.assign({}, DEFAULT_STATE);
      TodoMutations.doneTask(state, { data: DEFAULT_STATE.items[1]});
      assert.equal(state.items[1].is_do, false);
    });
  });
});
