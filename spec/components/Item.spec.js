import { mount } from '@lib/avoriaz';
import Item from '@/components/Item';
import assert from '@lib/power-assert';

// describeはテストをいずれかの単位で括るだけ
describe('Item.vue', () => {

  // "it"がJUnitでいうところの@Testみたいなもの
  it('タスク名がtitleであることを確認する', () => {
    // vueコンポーネントをpropsDataを引数に仮想DOMとして描画して、描画内容が正しいかを確認する
    const wrapper = mount(Item, {propsData: {item: {title: 'Task1', is_do: true}}});
    assert.equal(wrapper.text(), 'Task1');
  });

  it('is_doがtrueのとき、is_doクラスが付与されることを確認する', () => {
    const wrapper = mount(Item, {propsData: {item: {title: 'Task1', is_do: true}}});
    assert.equal(wrapper.getAttribute('class'), 'is-do');
  });

  it('is_doがfalseのとき、クラスが付与されないことを確認する', () => {
    const wrapper = mount(Item, {propsData: {item: {title: 'Task1', is_do: false}}});
    assert.equal(wrapper.getAttribute('class'), '');
  });
});
