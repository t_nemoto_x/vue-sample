/**
 * コンポーネントから呼び出されるアクションです。
 * サーバサイドへの問い合わせなどがあれば記述する。
 */
export const TodoActions = {

  /**
   * 新規タスクを追加する
   * @param commit ミューテーションハンドラ
   * @param title タスク名
   */
  addTask({commit}, title) {
    let newItem = {
      title: title,
      is_do: false
    };

    commit('addTask', {
      data: newItem
    });
  },

  /**
   * タスクを完了する
   * @param commit ミューテーションハンドラ
   * @param item 完了にするタスク
   */
  doneTask({commit}, item) {
    commit('doneTask', {
      data: item
    });
  }
};
