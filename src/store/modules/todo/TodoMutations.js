/**
 * MutationはActionから呼び出され、stateを操作します。
 * サーバサイドから取得した情報の処理ロジックを記述する。
 */
export const TodoMutations = {

  /**
   * 新規タスクを追加する
   * @param state ステート
   * @param payload 受け渡されるデータ
   */
  addTask(state, payload) {
    state.items.push(payload.data);
  },

  /**
   * タスクを完了にする
   * @param state ステート
   * @param payload 受け渡されたデータ
   */
  doneTask(state, payload) {
    let index = state.items.indexOf(payload.data);
    state.items[index].is_do = !payload.data.is_do;
  }
};
