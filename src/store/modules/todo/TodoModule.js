import { TodoActions } from './TodoActions';
import { TodoMutations } from './TodoMutations';

/**
 * Todo機能を管理するモジュールです。
 */
export const TodoModule = {
  todo: {
    // todoという名前空間でactionやmutationなどを登録する設定
    namespaced: true,
    // コンポーネントの描画をつかさどる状態オブジェクト
    state: {
      items: [
        {is_do: false, title: 'タスク1', id: 1},
        {is_do: true, title: 'タスク2', id: 2},
        {is_do: false, title: 'タスク3', id: 3}
      ]
    },
    // コンポーネントから呼び出されるaction
    actions: TodoActions,
    // stateを操作するmutation
    mutations: TodoMutations
  }
};
