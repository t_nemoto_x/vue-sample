/**
 * MutationはActionから呼び出され、stateを操作します。
 * サーバサイドから取得した情報の処理ロジックを記述する。
 */
export const AvaterMutations = {

  /**
   * 選択したユーザのアバターを更新する
   * @param state ステート
   * @param payload 受け渡されるデータ
   */
  updateAvater(state, payload) {
    state.url = payload.userinfo.avaterUrl;
  },
};
