import { AvaterActions } from './AvaterActions';
import { AvaterMutations } from './AvaterMutations';

/**
 * Todo機能を管理するモジュールです。
 */
export const AvaterModule = {
  avater: {
    // avaterという名前空間でactionやmutationなどを登録する設定
    namespaced: true,
    // コンポーネントの描画をつかさどる状態オブジェクト
    state: {
      url: ''
    },
    // コンポーネントから呼び出されるaction
    actions: AvaterActions,
    // stateを操作するmutation
    mutations: AvaterMutations
  }
};
