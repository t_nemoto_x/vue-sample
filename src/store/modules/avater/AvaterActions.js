import axios from 'axios';

/**
 * コンポーネントから呼び出されるアクションです。
 * サーバサイドへの問い合わせなどがあれば記述する。
 */
export const AvaterActions = {

  /**
   * ユーザを選択する
   * @param commit ミューテーションハンドラ
   * @param title タスク名
   */
  selectUser({commit}, username) {
    axios.get(`http://localhost:3000/userinfo?username=${username}`)
      .then((res) => {
        console.log(res);
        let userinfo = res.data[0];
        commit('updateAvater', {
          userinfo
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
