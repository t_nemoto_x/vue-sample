import Vue from 'vue';
import Vuex from 'vuex';
import { TodoModule } from './modules/todo/TodoModule';
import { AvaterModule } from './modules/avater/AvaterModule';
Vue.use(Vuex);

// 画面を構成するコンポーネントごとにモジュールを管理する
// 共通系のactionやmutationがあればここで定義してもよいかもしれない
export default new Vuex.Store({
  modules: Object.assign(TodoModule, AvaterModule)
});
