import Vue from 'vue';
import App from './App.vue';
import store from './store';

new Vue({
  // vuexのストアをインポートして設定
  store,
  // App.vueを割り当てる領域
  el: '#app',
  render: h => h(App)
});
