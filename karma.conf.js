const path = require('path');

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha'],

    // list of files / patterns to load in the browser
    files: [
      './spec/**/*.spec.js',
      './node_modules/phantomjs-polyfill-object-assign/object-assign-polyfill.js',
      './node_modules/phantomjs-polyfill-includes/includes-polyfill.js'
    ],

    // list of files to exclude
    exclude: ['**/*.swp'],

    plugins: [
      'karma-chrome-launcher', 'karma-mocha', 'karma-webpack', 'karma-coverage', 'karma-mocha-reporter', 'karma-junit-reporter', 'karma-babel-preprocessor'
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'spec/**/*.spec.js': ['babel', 'webpack'],
      'src/**/*.js': ['babel', 'coverage']
    },

    webpack: {
      // devtool: 'inline-source-map',
      entry: 'babel-polyfill',
      module: {
        exprContextCritical: false,
        rules: [
          {
            test: /\.css$/,
            use: [
              'vue-style-loader',
              'css-loader'
            ],
          }, {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
              loaders: {}
              // other vue-loader options go here
            }
          },
          {
            test: /.js$/,
            exclude: /node_modules/,
            use: 'babel-loader',
          },
          {
            test: /.json$/,
            use: 'json-loader'
          }, {
            test: /.js$/,
            exclude: /(spec|node_modules)/,
            use: 'istanbul-instrumenter-loader',
            enforce: 'post'
          }
        ]
      },
      node: {
        fs: 'empty'
      },
      resolve: {
        alias: {
          '@': path.resolve(__dirname, 'src'),
          '@test': path.resolve(__dirname, 'spec'),
          '@lib': path.resolve(__dirname, 'node_modules'),
        },
        extensions: ['.js', '.vue', '.css']
      }
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [
      'mocha', 'coverage', 'junit'
    ],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,
    coverageReporter: {
      dir: 'coverage/',
      reporters: [
        {
          type: 'html'
        },
        {
          type: 'text'
        },
        {
          type: 'cobertura',
          file: './build/reports/karma/coverage.xml'
        }
      ]
    },

    junitReporter: {
      outputDir: './build/reports/karma', // results will be saved as $outputDir/$browserName.xml
      outputFile: 'junit.xml'
    }
  });
};
